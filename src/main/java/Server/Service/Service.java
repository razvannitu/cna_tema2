package Server.Service;

import Server.StarSignsMap;
import io.grpc.stub.StreamObserver;
import resouces.protos.zodiac.StarSignServiceGrpc;
import resouces.protos.zodiac.StarSignRequest;
import resouces.protos.zodiac.StarSignResponse;

public class Service extends StarSignServiceGrpc.StarSignServiceImplBase {
    private final StarSignsMap starSignsMap;

    public Service() {
        starSignsMap = new StarSignsMap();
    }
    @Override
    public void getStarSign(StarSignRequest request, StreamObserver<StarSignResponse> responseObserver) {
        System.out.println("StarSign Service called");
        StarSignResponse.Builder response = StarSignResponse.newBuilder();
        response.setStarSign(starSignsMap.determineStarSign(request.getDate()));
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }
}