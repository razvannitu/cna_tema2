package Client;

import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import resouces.protos.zodiac.StarSignServiceGrpc;
import resouces.protos.zodiac.StarSignRequest;
import resouces.protos.zodiac.StarSignResponse;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
    private static final Logger logger = Logger.getLogger(Client.class.getName());

    private final StarSignServiceGrpc.StarSignServiceBlockingStub blockingStub;

    public Client(Channel channel) {
        blockingStub = StarSignServiceGrpc.newBlockingStub(channel);
    }

    public void starSign(String date) {
        logger.info("Will try to get " + date + " star sign.");
        StarSignRequest request = StarSignRequest
                .newBuilder()
                .setDate(date)
                .build();
        StarSignResponse response;
        try {
            response = blockingStub.getStarSign(request);
        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            return;
        }
        logger.info("Simbolul astrologic asociat cu data primita este: " + response);
    }

    public static class DateValidator{
        private final DateTimeFormatter dateFormatter;

        public DateValidator(DateTimeFormatter dateFormatter) {
            this.dateFormatter = dateFormatter;
        }

        public boolean isValidFormat(String dateStr) {
            LocalDate localDate;
            try {
                localDate = LocalDate.parse(dateStr, this.dateFormatter);
            } catch (DateTimeParseException e) {
                return false;
            }
            return true;
        }

        public boolean existingDate (String dateStr) {
            String[] dateArray = dateStr.split("/", 3);
            int[] dateArrayInt = new int[3];

            for (int index = 0; index < 3; index++) {
                dateArrayInt[index] = Integer.parseInt(dateArray[index]);
            }

            if (dateArrayInt[0] == 2) {
                if (dateArrayInt[2] % 4 == 0) {
                    if (dateArrayInt[1] > 29) {
                        return false;
                    }
                }
                else {
                    if (dateArrayInt[1] > 28) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    public static void main(String[] args) throws Exception {
        String date;
        String target = "localhost:50051";

        ManagedChannel channel = ManagedChannelBuilder.forTarget(target)
                .usePlaintext()
                .build();

        boolean onClient = true;
        while (onClient) {
            System.out.println("Introduceti data calendaristica: ");
            Scanner scanner = new Scanner(System.in);
            date = scanner.nextLine();

            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("M/d/yyyy");
            DateValidator validator = new DateValidator(dateTimeFormatter);


            Client client = new Client(channel);
            if (validator.isValidFormat(date)) {
                if (validator.existingDate(date)) {
                    client.starSign(date);
                }
                else {
                    System.out.println("Format corect. Data inexistenta. Incercati din nou:");
                }
            }
            else {
                System.out.println("Tip de data invalid. Formatul acceptat: MM/dd/yyyy. Incercati din nou:");
            }
        }
        channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
    }
}